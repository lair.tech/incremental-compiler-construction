# DEVLOG

# 2021-05-26
## 01 - Integers
Implemented the integer only language. Nothing fancy just generate a assembley file to with we `emit` the `scheme_entry` function header and then  `movl` the constant integer into `eax` and return (`ret`).

# 2021-05-27
## 02 - Immediate Constants
Added the immediates `boolean`, `()` and `char` in addition to the `fixnum`. The tagged types have the following tags, masks and shifts:
* fixnum = `<value>00`
  * tag   = `00`
  * mask  = `11`
  * shift = 2
* char = `<char>00001111`
  * tag   = `1111`
  * mask  = `11111111`
  * shift = 8
The constants have the follwing values:
* `#t = 10011111`
* `#f =    11111`
* `() =   101111` 

Also changed the `test-driver.scm` that it will generate the external datum representation first with `put-datum` before generating the test string with `(format "~a\n" x)` to compare the to. Otherwise the chars will not have the proper format. That way `#\tab` will be `#\tab\n` for the comparsion. Plus some minor renaming of the test functions because we don't compare to strings.

# 2021-05-28
## 03 - Unary Primitives
Added the unary primitives `fxadd1`, `fxsub1`, `fixnum->char`, `char->fixnum`, `fxlognot`, `null?`, `fxzero?`, `not`, `fixnum?`, `boolean?` and `char?` and their tests. For that i used the `define-primitive` (defined in the extended tutorial {add link}) and helper functions to define all the primitives instead of the nested `cond`s that are purposed in the example. The immediate and the primcalls also get there own emit functions `emit-immediate` and `emit-primcall` in the new `emit-expr` function respectivly.

# 2021-06-03
## 04 - Binary Primitives
Added the binary primitives `fx+`, `fx-`, `fx*`, `fxlogand`, `fxlogor`, `fx=`, `fx<`, `fx<=`, `fx>`, `fx>=`, `char=?`, `char<?`, `char<=?`, `char>?` and `char>=?` and their tests. Also moved all primitive function stuff into the file `primitives.scm`.

For evaluating multiple arguments we needed to add a stack management:
* evaluate the first argument and place it onto the stack index `si`
* evaluate the next argument with the next stack index (so it won't overwrite our saved value)
* then perform the operation with the saved value in the stack and the value in `eax`

The `emit-expr` and all primitives now also take the stack index `si` as additional argument. And added the helper function `next-stack-index`, `emit-stack-save` and `emit-stack-load`. We also don't reuse the c stack but instead create our own in the `runtime.c` and pass it to the `scheme_entry` function that now first save the orginal stack location into `exc` and then set our stack as the new `esp`. The new `L_scheme_entry` function is now will emit the code and will be called by the `scheme_entry` function. Before we return from `scheme_entry` we also restore the original stack.

## 05 - Local Variables
Extended the `emit-expr` with an enviroment that contains the local variable names to their stack locations. The let form will evaluate it's bindings one at a time and place the result into the next free stack index and also add the variable with their stack index location to the enviroment. So a lookup of a varibale is just a stack load of value at the stack index `si` from the enviroment mapping into `eax`.

## 06 - Conditional Expressions
Basically just a compy from the paper with some minor modifictions. Overall it will just emit the test and compare it to `#f` and then `je` (`jump equal`) to the alternative part other wise it will execute the consequence part and jump to the end before executing the alternative part.

# 2021-06-04
## 07 - Heap Allocation
### Pairs
Implemented the primitives `pair?`, `cons`, `car`, `cdr`, `set-car!` and `set-cdr!` for the list/pair handling. For that to work we added a heap allocation in the runtime and pass it into `scheme_entry` that now also get a additional context struct and makes a full context save/restore around our `L_scheme_entry`. The `runtime.c` print function also get a bit of refactoring to implement and handle lists/pairs.

To make the tests pass we also implemented `begin` and implicit begin in the `let` form. For the tests we additionally implemented the `eq?` primitive. Don't know why the tests want it because in the paper the `begin` or implicit begin is never mentioned and the given example codes also handle only one expression for `let` but anyways it's ok.

### Vectors
Implemented the primitives `vector?`, `make-vector`, `vector-length`, `vector-ref` and `vector-set!`. Nothing special here execpt the aligment calculation to the next 8 byte boundary:
* first we add the word-size for the size
* then we add another `2 * word size - 1`
* and last we cut of the last 3 bits so it aligned to the 8 byte boundary
The trick is that we are adding an additional 1 less then the 8 byte boundary (`7`) additonaly to the size of `4` so that when we cut of the last 3 bits it will be aligned to 8 bytes.

### Strings
Implemented the primitives `string?`, `make-string`, `string-length`, `string-ref` and `string-set!`. They are basically the same as the vector variants except for the tagging and that the size of the elements is only 1 byte.

# 2021-06-11
## 08 - Procedure Calls
First i made a bugfix that every primitive evaluate all it's arguments first so that there should be no more evaluation nesting bugs that used the same registers. That was an issue before that wasn't covered by the tests. For that i changed the `define-primitive` syntax macro that it will emit all it's arguments to the stack first except the last one that will be placed into `eax`.
