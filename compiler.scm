(load "test/test-driver.scm")
(load "test/tests-07.scm")
(load "test/tests-06.scm")
(load "test/tests-05.scm")
(load "test/tests-04.scm")
(load "test/tests-03.scm")
(load "test/tests-02.scm")
(load "test/tests-01.scm")

(load "primitives.scm")

;; tagged values
(define fixnum-tag   #x00)
(define fixnum-mask  #x03)
(define fixnum-shift    2)
(define char-tag     #x0F)
(define char-mask    #xFF)
(define char-shift      8)
;; constants
(define bool-f       #x1F)
(define bool-t       #x9F)
(define bool-mask    #x7F)
(define bool-shift      7)
(define empty-list   #x2F)
;; objects
(define object-mask  #x07)
(define pair-tag     #x01)
(define vector-tag   #x05)
(define string-tag   #x06)

(define word-size     4)

;; immediate stuff
(define (fixnum? expr)
  (define fixnum-bits  (- (* 8 word-size)  fixnum-shift))
  (define fixnum-upper (sub1 (expt 2 (- fixnum-bits 1))))
  (define fixnum-lower (- (expt 2 (- fixnum-bits 1))))
  (and (integer? expr) (<= expr fixnum-upper) (>= expr fixnum-lower)))

(define (immediate? expr)
  (or (fixnum? expr)
      (char? expr)
      (boolean? expr)
      (null? expr)))

(define (immediate-rep expr)
  (cond
   [(fixnum? expr)  (ash expr fixnum-shift)]
   [(char? expr)    (+ (ash (char->integer expr) char-shift) char-tag)]
   [(boolean? expr) (if expr bool-t bool-f)]
   [(null? expr)    empty-list]
   [else (error 'immediate-rep (format "unkown immediate: ~a" expr))]))

(define (emit-immediate expr)
  (emit "    movl $~a, %eax" (immediate-rep expr)))

;; stack helper functions
(define (emit-stack-save si . reg)
  (if (null? reg)
      (emit "    movl %eax, ~a(%esp)" si)
      (emit "    movl %~s, ~a(%esp)" (car reg) si)))

(define (emit-stack-load si . reg)
  (if (null? reg)
      (emit "    movl ~a(%esp), %eax" si)
      (emit "    movl ~a(%esp), %~s"  si (car reg))))

(define (stack-offset si offset)
  (- si (* word-size offset)))

(define (tagged-expr? expr tag . expr-length)
  (and (not (null? expr))
       (equal? (car expr) tag)
       (or (null? expr-length)
           (equal? (length expr) (car expr-length)))))

(define variable? symbol?)
(define (extend-env var si env)
  (cons (cons var si) env))

(define (emit-variable env expr)
  (cond
   [(assoc expr env)
    (emit-stack-load (cdr (assoc expr env)))]
   [else (error 'emit-variable (format "Non bound var: ~s" expr))]))

(define (let? expr) (tagged-expr? expr 'let))
(define let-bindings cadr)
(define let-body cddr)

(define (emit-let si env bindings body)
  (cond
   [(null? bindings) (emit-begin si env body)]
   [else
    (let ([binding (car bindings)])
      (emit-expr si env (cadr binding))
      (emit-stack-save si)
      (emit-let (stack-offset si 1) (extend-env (car binding) si env) (cdr bindings) body))]))

(define unique-label
  (let ([counter 0])
    (lambda (name)
      (let ([label (format "~a_~a" name counter)])
        (set! counter (+ counter 1))
        label))))

(define (emit-label name)
  (emit "~a:" name))

(define (if? expr) (tagged-expr? expr 'if 4))
(define if-test cadr)
(define if-conseq caddr)
(define if-altern cadddr)

(define (emit-if si env test conseq altern)
  (let ([label-alt (unique-label "if_alt")]
        [label-end (unique-label "if_end")])
    (emit-expr si env test)
    (emit "    cmp $~a, %eax" (immediate-rep #f))
    (emit "    je ~a" label-alt)
    (emit-expr si env conseq)
    (emit "    jmp ~a" label-end)
    (emit-label label-alt)
    (emit-expr si env altern)
    (emit-label label-end)))

(define (begin? expr) (tagged-expr? expr 'begin))
(define begin-body cdr)
(define (emit-begin si env body)
  (unless (null? body)
    (emit-expr si env (car body))
    (emit-begin si env (cdr body))))

(define (emit-expr si env expr)
  (emit "# begin: ~s" expr)
  (cond
   [(immediate? expr) (emit-immediate expr)]
   [(variable? expr)  (emit-variable env expr)]
   [(primcall? expr)  (emit-primcall si env expr)]
   [(let? expr)       (emit-let si env (let-bindings expr) (let-body expr))]
   [(if? expr)        (emit-if si env (if-test expr) (if-conseq expr) (if-altern expr))]
   [(begin? expr)     (emit-begin si env (begin-body expr))]
   [else (error 'emit-expr (format "unkown expr: ~a" expr))])
  (emit "# end:   ~s" expr))

(define (emit-function-header name)
  (emit "    .text")
  (emit "    .globl ~a" name)
  (emit "    .type ~a, @function" name)
  (emit-label name))

(define (compile-program expr)
  (emit-function-header "L_scheme_entry")
  (emit-expr (stack-offset 0 1) '() expr)
  (emit "ret")
  (emit-function-header "scheme_entry")
  (emit "    movl 4(%esp), %ecx")   ; move context adress to ecx
  (emit "    movl %ebx, 4(%ecx)")   ; store ebx
  (emit "    movl %esi, 16(%ecx)")  ; store esi
  (emit "    movl %edi, 20(%ecx)")  ; store edi
  (emit "    movl %ebp, 24(%ecx)")  ; store ebp
  (emit "    movl %esp, 28(%ecx)")  ; store esp
  (emit "    movl 12(%esp), %esi")  ; move heap adress to esi
  (emit "    movl 8(%esp), %esp")   ; at least move stack-top adress to esp
  (emit "    call L_scheme_entry")
  (emit "    movl 4(%ecx), %ebx")   ; restore ebx
  (emit "    movl 16(%ecx), %esi")  ; restore esi
  (emit "    movl 20(%ecx), %edi")  ; restore edi
  (emit "    movl 24(%ecx), %ebp")  ; restore ebp
  (emit "    movl 28(%ecx), %esp")  ; restore esp
  (emit "    ret"))
