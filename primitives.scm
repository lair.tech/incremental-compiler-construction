
;; primcall stuff
(define-syntax define-primitive
  (syntax-rules ()
    [(_ (prim-name si env arg arg* ...) b b* ...)
     (begin
       (putprop 'prim-name '*is-prim*   #t)
       (putprop 'prim-name '*arg-count* (length '(arg arg* ...)))
       (putprop 'prim-name '*emitter*   (lambda (si env arg arg* ...)
                                          (emit-args si env (list arg arg* ...))
                                          b b* ...)))]))

(define (emit-args si env args)
  (unless (null? args)
    (emit-expr si env (car args))
    (when (> (length args) 1)
      (emit-stack-save si))
    (emit-args (stack-offset si 1) env (cdr args))))

(define (primitive? expr)
  (and (symbol? expr) (getprop expr '*is-prim*)))

(define (primitive-emitter expr)
  (or (getprop expr '*emitter*) (error 'primitive-emitter (format "unkown primitive: ~s" expr))))

(define (primcall? expr)
  (and (pair? expr) (primitive? (car expr))))

(define (emit-primcall si env expr)
  (let ([prim (car expr)]
        [args (cdr expr)])
    (check-primcall-args prim args)
    (apply (primitive-emitter prim) si env args)))

(define (check-primcall-args prim args)
  (let ([args-length (length args)]
        [prim-length (getprop prim '*arg-count*)])
    (or (= args-length prim-length)
        (error 'check-primcall-args
               (format "wrong number of arguments for ~s. ~s instead of ~s" prim args-length prim-length)))))

;; unary primitives
(define (emit-op-on-constant operator constant)
  (emit "    ~s $~a, %eax" operator constant))

(define-primitive (fxadd1 si env arg)
  (emit-op-on-constant 'addl (immediate-rep 1)))

(define-primitive (fxsub1 si env arg)
  (emit-op-on-constant 'subl (immediate-rep 1)))

(define-primitive (fixnum->char si env arg)
  (emit-op-on-constant 'shll (- char-shift fixnum-shift))
  (emit-op-on-constant 'addl char-tag))

(define-primitive (char->fixnum si env arg)
  (emit-op-on-constant 'shrl (- char-shift fixnum-shift)))

(define-primitive (fxlognot si env arg)
  (emit-op-on-constant 'shrl fixnum-shift)
  (emit "    not %eax")
  (emit-op-on-constant 'shll fixnum-shift))

;; unary comparsion primitives
(define (emit-setx-boolean setx)
  (emit "    ~s %al" setx)
  (emit "    movzbl %al, %eax")
  (emit "    sal $~s, %al" bool-shift)
  (emit "    or $~s, %al" bool-f))

(define (emit-cmp-boolean to setx)
  (emit "    cmp $~s, %eax" to)
  (emit-setx-boolean setx))

(define-primitive (null? si env arg)
  (emit-cmp-boolean empty-list 'sete))

(define-primitive (fxzero? si env arg)
  (emit-cmp-boolean 0 'sete))

(define-primitive (not si env arg)
  (emit-cmp-boolean bool-f 'sete))

(define-primitive (fixnum? si env arg)
  (emit-op-on-constant 'andl fixnum-mask)
  (emit-cmp-boolean fixnum-tag 'sete))

(define-primitive (boolean? si env arg)
  (emit-op-on-constant 'andl bool-mask)
  (emit-cmp-boolean bool-f 'sete))

(define-primitive (char? si env arg)
  (emit-op-on-constant 'andl char-mask)
  (emit-cmp-boolean char-tag 'sete))

(define (emit-op-on-stack-index si operator)
  (emit "    ~s ~a(%esp), %eax" operator si))

(define-primitive (fx+ si env arg1 arg2)
  (emit-op-on-stack-index si 'addl))

(define-primitive (fx- si env arg1 arg2)
  (emit "    movl %eax, %edx")
  (emit-stack-load si)
  (emit "    subl %edx, %eax"))

(define-primitive (fx* si env arg1 arg2)
  (emit "    shrl $~a, %eax" fixnum-shift)
  (emit "    shrl $~a, ~a(%esp)" fixnum-shift si)
  (emit "    imul ~a(%esp), %eax" si)
  (emit "    shll $~a, %eax" fixnum-shift))

(define-primitive (fxlogor si env arg1 arg2)
  (emit-op-on-stack-index si 'orl))

(define-primitive (fxlogand si env arg1 arg2)
  (emit-op-on-stack-index si 'andl))

(define (emit-cmp-stack-index si setx)
  (emit-op-on-stack-index si 'cmp)
  (emit-setx-boolean setx))

(define-primitive (fx= si env arg1 arg2)
  (emit-cmp-stack-index si 'sete))

(define-primitive (fx< si env arg1 arg2)
  (emit-cmp-stack-index si 'setg))

(define-primitive (fx<= si env arg1 arg2)
  (emit-cmp-stack-index si 'setge))

(define-primitive (fx> si env arg1 arg2)
  (emit-cmp-stack-index si 'setl))

(define-primitive (fx>= si env arg1 arg2)
  (emit-cmp-stack-index si 'setle))


(define-primitive (char=? si env arg1 arg2)
  (emit-cmp-stack-index si 'sete))

(define-primitive (char<? si env arg1 arg2)
  (emit-cmp-stack-index si 'setg))

(define-primitive (char<=? si env arg1 arg2)
  (emit-cmp-stack-index si 'setge))

(define-primitive (char>? si env arg1 arg2)
  (emit-cmp-stack-index si 'setl))

(define-primitive (char>=? si env arg1 arg2)
  (emit-cmp-stack-index si 'setle))

(define-primitive (eq? si env arg1 arg2)
  (emit-cmp-stack-index si 'sete))

;; pair stuff
(define-primitive (pair? si env arg)
  (emit-op-on-constant 'andl object-mask)
  (emit-cmp-boolean pair-tag 'sete))

(define-primitive (cons si env arg1 arg2)
  (emit "    movl %eax, 4(%esi)")
  (emit-stack-load si)
  (emit "    movl %eax, 0(%esi)")
  (emit "    movl %esi, %eax")
  (emit "    orl  $~a,  %eax" pair-tag)
  (emit "    addl $~a,  %esi" (* word-size 2)))

(define-primitive (car si env arg)
  (emit "    movl -1(%eax), %eax"))

(define-primitive (cdr si env arg)
  (emit "    movl 3(%eax),  %eax"))

(define-primitive (set-car! si env pair value)
  (emit "    movl %eax, %edx")
  (emit-stack-load si)
  (emit "    movl %edx, -1(%eax)"))

(define-primitive (set-cdr! si env pair value)
  (emit "    movl %eax, %edx")
  (emit-stack-load si)
  (emit "    movl %edx, 3(%eax)"))

;; vector stuff
(define-primitive (vector? si env arg)
  (emit-op-on-constant 'andl object-mask)
  (emit-cmp-boolean vector-tag 'sete))

(define-primitive (make-vector si env size)
  (emit "    movl %eax, 0(%esi)")
  (emit "    movl %eax, %edx")
  (emit "    movl %esi, %eax")
  (emit "    orl  $~a, %eax" vector-tag)
  (emit "    shrl $~a, %edx" fixnum-shift)
  (emit "    imul $~a, %edx" word-size)
  (emit "    addl $~a, %edx" (+ word-size (- (* 2 word-size) 1))) ; 11 = size + offset to align to next 8 byte boundary
  (emit "    andl $-8, %edx") ; with the cutting of the last 3 bits
  (emit "    addl %edx, %esi"))

(define-primitive (vector-length si env vec)
  (emit "    movl -~a(%eax), %eax" vector-tag))

(define-primitive (vector-ref si env vec index)
  (emit "    shrl $~a, %eax" fixnum-shift)
  (emit "    imul $~a, %eax" word-size) ;; each entry has a full word size
  (emit "    addl $~a, %eax" word-size) ;; size of vector field
  (emit-stack-load si 'edx) ;; move vec to edx
  (emit "    addl %eax, %edx") ;; and add offset
  (emit "    movl -~a(%edx), %eax" vector-tag))

(define-primitive (vector-set! si env vec index value)
  (emit-stack-save (stack-offset si 2)) ; save value
  (emit-stack-load (stack-offset si 1))
  (emit "    shrl $~a, %eax" fixnum-shift)
  (emit "    imul $~a, %eax" word-size) ;; each entry has a full word size
  (emit "    addl $~a, %eax" word-size) ;; size of vector field
  (emit-stack-load si 'edx)
  (emit "    addl %eax, %edx") ;; offset
  (emit-stack-load (stack-offset si 2)) ; load value
  (emit "    movl %eax, -~a(%edx)" vector-tag))

;; string stuff
(define-primitive (string? si env arg)
  (emit-op-on-constant 'andl object-mask)
  (emit-cmp-boolean string-tag 'sete))

(define-primitive (make-string si env size)
  (emit "    movl %eax, 0(%esi)")
  (emit "    movl %eax, %edx")
  (emit "    movl %esi, %eax")
  (emit "    orl  $~a, %eax" string-tag)
  (emit "    shrl $~a, %edx" fixnum-shift)
  (emit "    addl $~a, %edx" (+ word-size (- (* 2 word-size) 1))) ; 11 = size + offset to align to next 8 byte boundary
  (emit "    andl $-8, %edx") ; with the cutting of the last 3 bits
  (emit "    addl %edx, %esi"))

(define-primitive (string-length si env str)
  (emit "    movl -~a(%eax), %eax" string-tag))

(define-primitive (string-ref si env str index)
  (emit "    shrl $~a, %eax" fixnum-shift)
  (emit "    addl $~a, %eax" word-size) ; add size to offset
  (emit-stack-load si 'edx)
  (emit "    addl %eax, %edx")
  (emit "    movzbl -~a(%edx), %eax" string-tag)
  (emit "    shll $~a, %eax" char-shift)
  (emit "    addl $~a, %eax" char-tag))

(define-primitive (string-set! si env str index char)
  (emit-stack-save (stack-offset si 2)) ; save char
  (emit-stack-load (stack-offset si 1)) ; load index
  (emit "    shrl $~a, %eax" fixnum-shift)
  (emit "    addl $~a, %eax" word-size) ; add size to offset
  (emit-stack-load si 'edx)
  (emit "    addl %eax, %edx")
  (emit-stack-load (stack-offset si 2))
  (emit "    shrl $~a, %eax" char-shift)
  (emit "    movb %al, -~a(%edx)" string-tag))
