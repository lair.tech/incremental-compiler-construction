#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdbool.h>

// tag values
#define fixnum_tag    0x00
#define fixnum_mask   0x03
#define fixnum_shift     2
#define char_tag      0x0F
#define char_mask     0xFF
#define char_shift       8
// constants
#define bool_f        0x1F
#define bool_t        0x9F
#define empty_list    0x2F
// objects
#define object_mask   0x07
#define pair_tag      0x01
#define vector_tag    0x05
#define string_tag    0x06

#define word_size        4

typedef unsigned int ptr;

typedef struct {
  ptr car;
  ptr cdr;
} pair;

bool is_pair(ptr value) {
  return ((value & object_mask) == pair_tag);
}

ptr pair_car(ptr value) {
  return ((pair*) (value - pair_tag))->car;
}

ptr pair_cdr(ptr value) {
  return ((pair*) (value - pair_tag))->cdr;
}

void print_expr(ptr value);

void print_list_content(ptr value) {
  if (!is_pair(value)) {
    print_expr(value);
    return;
  }

  ptr car = pair_car(value);
  print_expr(car);

  ptr cdr = pair_cdr(value);
  if (cdr == empty_list) {
    return;
  }

  printf(" ");

  if(!is_pair(cdr)) {
    printf(". ");
  }

  print_list_content(cdr);
}

void print_char(char value) {
  if (value == '\n') {
    printf("#\\newline");
  } else if (value == '\r') {
    printf("#\\return");
  } else if (value == '\t') {
    printf("#\\tab");
  } else if (value == ' ') {
    printf("#\\space");
  } else {
    printf("#\\%c", value);
  }
}

void print_expr(ptr value) {
  if ((value & fixnum_mask) == fixnum_tag) {
    printf("%d", ((int) value) >> fixnum_shift);
  } else if ((value & char_mask) == char_tag) {
    print_char((char) (value >> char_shift));
  } else if (is_pair(value)) {
    printf("(");
    print_list_content(value);
    printf(")");
  } else if ((value & object_mask) == vector_tag) {
    int size = *(ptr*)(value - vector_tag) >> fixnum_shift;
    printf("#(");
    for (int i = 1; i <= size; i++) {
      print_expr(*(ptr*)(value - vector_tag + i * word_size));
      if (i != size) {
        printf(" ");
      }
    }
    printf(")");
  } else if ((value & object_mask) == string_tag) {
    int size = *(ptr*)(value - string_tag) >> fixnum_shift;
    printf("\"");
    for (int i = 0; i < size; i++) {
      char c =  *(ptr*)(value - string_tag + word_size + i);
      if (c == '\"') {
        printf("\\\"");
      } else if (c == '\\') {
        printf("\\\\");
      } else {
        printf("%c", c);
      }
    }
    printf("\"");
  } else if (value == bool_t) {
    printf("#t");
  } else if (value == bool_f) {
    printf("#f");
  } else if (value == empty_list) {
    printf("()");
  } else {
    printf("unkown value: %d", value);
  }
}

void print_expr_line(ptr value) {
  print_expr(value);
  printf("\n");
}

static char* allocate_protected_space(int size) {
  int page = getpagesize();
  int status;
  int aligned_size = ((size + page -1) / page) * page;
  char* p = mmap(0, aligned_size + 2 * page,
                 PROT_READ | PROT_WRITE,
                 MAP_ANONYMOUS | MAP_PRIVATE,
                 0, 0);
  if (p == MAP_FAILED) {
    printf("Failed to memory map the procted space");
  }
  status = mprotect(p, page, PROT_NONE);
  if (status != 0) {
    printf("Failed to memory protect the lower bound of the space");
  }
  status = mprotect(p + page + aligned_size, page, PROT_NONE);
  if (status != 0) {
    printf("Failed to memory protect the higher bound of the space");
  }
  return (p+page);
}

static void deallocate_protected_space(char* p, int size) {
  int page = getpagesize();
  int status;
  int aligned_size = ((size + page -1) / page) * page;
  status = munmap(p - page, aligned_size + 2 * page);
  if (status != 0) {
    printf("failed to deallocate memory protected space");
  }
}

typedef struct {
    void* eax;  /* 0   scratch */
    void* ebx;  /* 4   preserve */
    void* ecx;  /* 8   scratch */
    void* edx;  /* 12  scratch */
    void* esi;  /* 16  preserve */
    void* edi;  /* 20  preserve */
    void* ebp;  /* 24  preserve */
    void* esp;  /* 28  preserve */
} context;

ptr scheme_entry(context* context, char* stack_base, char* heap);

int main(int argc, char** argv) {
  // allocate stack space
  int stack_size = 16 * 4096; // 16k cells
  char* stack_top = allocate_protected_space(stack_size);
  char* stack_base = stack_top + stack_size;
  // allocate heap space
  int heap_size = 16 * 1024 * 4096; // 1.6m values
  char* heap = allocate_protected_space(heap_size);

  context context;
  print_expr_line(scheme_entry(&context, stack_base, heap));

  // deallocate stack and heap
  deallocate_protected_space(stack_top, stack_size);
  deallocate_protected_space(heap, heap_size);

  return 0;
}
