# Incremental compiler construction
Implementation of the paper `An Incremental Approach to Compiler Construction`.  It implements a basic compiler in 24 steps with the following features:
* lexically coped scheme (like) language
* tail calls         (10)
* assignments        (12)
* reader             (23)
* libaries           (14)
* ports
  * output ports     (19)
  * input ports      (21)
* immeditates values
  * integers         (01)
  * booleans         (02)
  * empty list       (02)
  * chars            (02)
  * symbols          (14)
* compound values
  * pairs (list)     (07)
  * vectors          (07)
  * strings          (07)
* applicators
  * primitives       (03 & 04)
  * procedures       (08)
  * closures         (09)
  * forgein funtions (15)
  
## Software needed
* gcc
* chez scheme (https://github.com/cisco/ChezScheme.git)

## Installing gcc and build tools
* Arch/Manjaro: `sudo pacman -Syu base-devel`

## Downloading Chez Scheme source and build it
* `git clone https://github.com/cisco/ChezScheme.git`
* `cd ChezScheme`
* `.configure`
* `sudo make install`

## Commit structure
Each commit with have a prefix based on the chapter. For example `3.1 Integers` will have the prefix `01 - <commit message>`

## Devlog
The developement log can be found under `DEVLOG.md`

## Project structure
* `compiler.scm` contains the compiler that emits out program as a `scheme-entry` function in `x86` gcc assembly
* `primitives.scm` contains the code emitter stuff for all primitives
* `runtime.c` c runtime to print the generated values that are returned by `scheme-entry`.
* `test.c` test file to generate the an example gcc assembly file with `gcc --omit-frame-pointer -fno-asynchronous-unwind-tables -S test.c` that that we use as a guide line for generating our assembly content. Will contain new stuff as needed.
* `test`
   * `test-driver.scm` contains the test utils for calling the compiler and building an executeable with the `runtime.c`. The resulting string that is printed by the `runtime.c` will be tested against the expected output.
   * test files for the chapters are named `test-<chaper-number>.scm`
* `doc`
   * `An-Incremental-Approach-to-Compiler-Construction.pdf` The paper that is implemented
* `DEVLOG.md` contains the devlog comments

